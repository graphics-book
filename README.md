# The Chrome Graphics Book

[TOC]

## About

This is the source code for a public book and living document capturing the high
level architecture and low-level details of graphics in Chrome and ChromeOS.

This book is currently written with https://rust-lang.github.io/mdBook/

## Special Features

This mdbook supports some homebrew markdown extensions. These are
provided by `scripts/preprocess-graphics-book.py`.

### Nomnoml diagrams

One can create in-line nomnoml diagrams by formatting a code block
like so:

    ``` diagram=nomnoml
    [foo] -> [bar]
    ```

### Text replacements

We also support special text replacements:

- $$GIT_REV$$ is replaced by the current git revision

## How To: Run the book locally

First, install mdbook following the guide [0].

Then, to build and run the book locally, run the command `mdbook serve`. This
will start a local server that you can connect to at `localhost:3000`.

[0] https://rust-lang.github.io/mdBook/guide/installation.html

## Deployment

The book is currently deployed at firebase. The public URL is
https://graphics-book.web.app.

With the appropriate permissions on the project, one can build and push the
book like so:

  $ mdbook build && firebase deploy

