# About

This book is built using with [mdbook](https://github.com/rust-lang/mdBook).

Diagrams were created with:

- [nomnoml](https://nomnoml.com/)
- [Google Drawings](https://docs.google.com/drawings/)

This book was built at revision: $$GIT_REV$$
