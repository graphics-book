# Chrome Architecture

Chrome is one of the biggest and most complex open source projects on the
planet. It's a project with millions of lines of code, hundreds of thousands of
files, and thousands of contributors, all built over the course of more than a
decade.

For someone trying to understand how Chrome is organized, all of that detail can
be overwhelming.

Fortunately, if you zoom out enough, you can see the forest for the trees, and
start to make sense of the project.

This section covers the important high-level details about how Chrome is
organized. In it, you will learn:

- The 10,000 foot view of how Chrome is organized
- How distinct processes and threads are used to achieve security, reliability,
  and concurrency
- How those processes and threads communicate
- How to lift the hood and see this organization in action

## Chrome on ChromeOS

As will become clear in this and the next section, ChromeOS is unique in how it
is set up.

Unlike on other OSes, Chrome on ChromeOS has historically run directly on top of
the kernel and managed all of the system UI. This means all the window
management, settings pages, etc. get bundled into the Chrome application.

This is changing with the
[LaCrOS project](https://chromium.googlesource.com/chromium/src.git/+/HEAD/docs/lacros.md),
which separates the browser and system UI applications. Both of these are built
from the same sources and are both structured as Chrome applcications, meaning
that on a LaCrOS-enabled Chromebook, you're running two full Chrome applications
simultaneously.

What you need to know for now is this: what's written in this section applies to
Chrome on other OSes, the historical set-up on ChromeOS, and for each of the
Chrome-based applcications in LaCrOS.
