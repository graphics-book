# Processes and Threads

Architecturally, a Chrome **program** is a collection of **specialized
processes**, each of which hosts **specialized threads**. We'll break down the
responsiblities of the processes here, and leave the details for the threads

``` diagram=nomnoml
#direction: right
[<abstract> Chrome |
  [browser process|
  	main thread |
    io thread |
    system threads
  ]
  [GPU process|
  	main thread |
    io thread |
    viz thread |
    compositor thread
  ]
  [render process 1|
  	main thread |
    io thread |
    cc thread |
    raster thread
  ]
  [render process 2|
  	main thread |
    io thread |
    cc thread |
    raster thread
  ]
  [render process N|
  	main thread |
    io thread |
    cc thread |
    raster thread
  ]
  [browser process] -> [GPU process]
  [browser process] -> [render process 1]
  [browser process] -> [render process 2]
  [browser process] -> [render process N]
  [render process 1] -> [GPU process]
  [render process 2] -> [GPU process]
  [render process N] -> [GPU process]
]
```

There are a few reasons for this choice:

- **Security**: most of the content Chrome handles is untrusted, so isolating
  the code that handles that content separate sandboxed makes users safer
- **Reliability**: Graphics code and rendering code is complicated and is built
  on top of even more complex systems. By doing graphics and processing work in
  a seperate process, crashes won't take down the whole browser and can be
  restarted automatically

## Browser Process

This process starts first, and it is home to the Chrome UI (tabs, omnibar). It
starts the other process and communicates with them over IPC to coordinate work.
Less sandboxed than other processes, can do things like read and write files.

## Render Processes

These processes manage instances of blink and run untrusted code from the
internet. They are highly sandboxed and need to lean on the host and GPU
processes to do more sensitive work.

There is one render process for every tab, and every frame in every tab.

## GPU Process (AKA Viz)

This process isolates sensitive graphics tasks from the renderer. It is highly
sandboxed, but has access to the graphics hardware on the device. It does things
like rasterization, compositing.

**Rasterization** is the process of converting raw structured data into images.

**Compositing** is, in short, the process of combining graphical information
from multiple sources into a final rendered image.
