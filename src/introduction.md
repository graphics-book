# Introduction

🥳 Welcome to the Chrome Graphics Book! 🥳

In this book, we're going to go on a whirlwind tour of the architecture of
Graphics in Chrome and ChromeOS.

There are several target audiences for this project:

- Engineers interested in understanding or working on graphics in
  Chrome/ChromeOS
- PMs, TPgMs, PgMs interested in getting a sense of the technical details of our
  graphics stack

## Organization

This document is divided into three major sections:

- **Broad Architecture and Big Boxes**\
  A general overview of the high-level architecture of Chrome/ChromeOS. Ideas, not
  code.
- **Foundational Concepts and Code**\
  Explanations of and pointers to key architectural principles and API surfaces.
- **How Things Really Work**\
  Detailed journeys through special topics in Chrome Graphics. Deep dives w/ lots
  of code.

## Contributing

If you have experience with graphics in ChromeOS and you would like to
contribute, we'd love your help in making this living document better!

The code behind this book lives in
[this github repo](https://chromium.googlesource.com/graphics-book).
Contributions can be made via the
[standard chromium gerrit workflow](https://www.chromium.org/developers/gerrit-guide/).
