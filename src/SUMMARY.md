# Summary

[Introduction](./introduction.md) [Background](./background.md)

# Broad Architecture and Big Boxes

- [Chrome Architecture](./chrome-architecture.md)
  - [Processes and Threads](chrome-architecture--processes-threads.md)
  - [IPC and APIs](chrome-architecture--ipc-api.md)
- [ChromeOS Architecture](./chromeos-architecture.md)

# Foundational Concepts and Code

- [Processes and Threads](./processes-threads.md)
- [Core ChromeOS APIs](./cros-apis.md)
  - [Wayland](./wayland.md)
  - [CrosAPI](./crosapi.md)
- [Skia](./skia.md)
- [Color Management](./color-management.md)

# How Things _Really_ Work

- [Compositing Web Content](./how-compositing.md)
- [Display Management](./how-displays.md)
- [Screenshots and Mirroring Windows](./how-screenshots.md)
- [SharedImages](./how-sharedimages.md)

---

[Contributors](./contributors.md) [About](./about.md)
