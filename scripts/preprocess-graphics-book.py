import json
import re
import subprocess
import sys

DIAGRAM_BLOCK_START_REGEX = re.compile(
    r"```\s*diagram\s*=\s*(?P<type>\w+)\s*")
UNIQUE_ID_COUNTER = 0


def error(*args, **kwargs):
  """Print to stderr

  Use this for debugging / messaging since the output of this script is
  directly parsed as a book by mdbook.
  """

  return print(*args, file=sys.stderr, **kwargs)


def ToNomnomlHtml(diagram_src):
  global UNIQUE_ID_COUNTER

  UNIQUE_ID_COUNTER += 1
  unique_id = f"diagram-id-{UNIQUE_ID_COUNTER}"

  return f"""
  <div id="{unique_id}">
    Nomnoml diagram missing. Check the console for more details.
  </div>
  <script>
    window.addEventListener('load', () => {{
      const el = document.getElementById("{unique_id}");
      const src = `{diagram_src}`;
      el.innerHTML = nomnoml.renderSvg(src);
      console.trace("Loaded!", "{unique_id}", src, nomnoml);
    }});
  </script>
  """


def TransformDiagramCodeBlock(diagram_type, code_block):
  if diagram_type == "nomnoml":
    return ToNomnomlHtml(code_block)

  raise Exception(
      f"Unable to translate diagram code block with type: {diagram_type}")


def TransformDiagramCodeBlocks(content):
  """Replaces diagram code blocks with the appropriate HTML/JS."""

  output = []
  diagram_type = None
  in_code_block = False
  code_block = []

  for line in content.split("\n"):
    if not in_code_block:
      match = DIAGRAM_BLOCK_START_REGEX.match(line)
      if match:
        in_code_block = True
        diagram_type = match.group("type")
      else:
        output.append(line)
    else:
      if line.startswith("```"):
        output.append(TransformDiagramCodeBlock(
            diagram_type, "\n".join(code_block)))
        in_code_block = False
        code_block = []
        diagram_type = None
      else:
        code_block.append(line)

  if in_code_block:
    raise Exception(
        f"Unfinished diagram code block found with type: {diagram_type}")

  return "\n".join(output)


def TransformPlaceholders(content):
  """Replaces special placeholders ($$FOO$$)."""

  git_revision = subprocess.run(
      ["git", "rev-parse", "HEAD"], capture_output=True, check=True, text=True).stdout.strip()

  return content.replace("$$GIT_REV$$", git_revision)


def ProcessBook(book):
  """Processes the book in-place, parsing chapters and applying changes."""

  def ProcessChapter(chapter):
    chapter["content"] = TransformDiagramCodeBlocks(chapter["content"])
    chapter["content"] = TransformPlaceholders(chapter["content"])
    for sub_item in chapter["sub_items"]:
      ProcessBookItem(sub_item)

  def ProcessBookItem(book_item):
    if "Chapter" in book_item:
      ProcessChapter(book_item["Chapter"])

  for book_item in book["sections"]:
    ProcessBookItem(book_item)


def main():
  # The first time the script is called, it's called with "supports <renderer>"
  if len(sys.argv) == 3 and sys.argv[1] == "supports":
    if sys.argv[2] == "html":
      return 0
    return 1

  unused_context, book = json.load(sys.stdin)
  ProcessBook(book)
  print(json.dumps(book))

  return 0


if __name__ == '__main__':
  sys.exit(main())
