# Copyright 2022 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# These presubmits run as part of the `git cl upload` workflow. Each "Check"
# method is run magically.
#
# See https://www.chromium.org/developers/how-tos/depottools/presubmit-scripts/
# for more information on how to write presubmits.

PRESUBMIT_VERSION = '2.0.0'

# This line is 'magic' in that git-cl looks for it to decide whether to
# use Python3 instead of Python2 when running the code in this file.
USE_PYTHON3 = True


def CheckBookBuilds(input_api, output_api):
  """Verifies that the book itself builds"""

  try:
    input_api.subprocess.check_output(["mdbook", "--help"])
  except OSError:
    return [output_api.PresubmitPromptWarning(
        "'mdbook' command is not available. Skipping build check. Proceed at "
        "your own risk!")]

  with input_api.tempfile.TemporaryDirectory() as out_dir:
    try:
      input_api.subprocess.check_output(
          ["mdbook", "build", f"-d={out_dir}"],
          stderr=input_api.subprocess.STDOUT)
    except input_api.subprocess.CalledProcessError as e:
      return [output_api.PresubmitError("Failed to build the book.",
                                        long_text=e.output)]

  return []
